extends Area2D

func _on_BigPellet_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("Fruit"):
		self.queue_free()
	if body.is_in_group("BigPellet") && body != $StaticBody2D:
		self.queue_free()
	if body.is_in_group("Player"):
		body.score += 10
		body.newBody()
		self.queue_free()
	pass
