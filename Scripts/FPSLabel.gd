extends Label

var fps_label = self

func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#Show FPS
	fps_label.set_text(str(Engine.get_frames_per_second()))
	if Input.is_action_just_pressed("ui_page_up"):
		if !fps_label.is_visible():
			fps_label.show()
		else:
			fps_label.hide()
	pass