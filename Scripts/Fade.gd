extends Node2D

var scene

var fTime = 2#1.75
var fadeInTime = 0.6#0.875
var fadeOutTime = 0.6#0.875

onready var panel = $Panel
onready var anim = $Anim
onready var timer = $Timer

func fadeOut(var scene2):
	timer.set_wait_time(fadeInTime)
	anim.play("Fade In",0,fTime)
	timer.start()
	get_tree().paused = true
	scene = scene2

func fadeOut2():
	timer.set_wait_time(fadeInTime)
	anim.play("Fade In",0,fTime)
	timer.start()
	get_tree().paused = true

func fadeIn():
	if Global.fixPos:
		self.set_position(Vector2(Global.dist,0))
	timer.set_wait_time(fadeOutTime)
	anim.play("Fade Out",0,fTime)
	timer.start()
	get_tree().paused = true

func _on_Timer_timeout():
	get_tree().paused = false
	
	#Change scene
	if scene != null:
		if scene == "Quit":
			get_tree().quit()
		else:
			get_tree().change_scene(scene)
			scene = null
	pass
