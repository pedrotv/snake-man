extends Area2D

func _ready():
	connect("body_shape_entered",self,"_on_Area_body_shape_entered")

func _on_Area_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("Player"):
		body.score += 10
		self.queue_free()
		body.newBody()
	pass