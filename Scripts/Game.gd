extends Node2D

onready var gameOver = $GameOver 

onready var map = $Map

onready var player1 = $Player1
onready var player2 = $Player2

var p1wr
var p2wr
var powerupWR

var fruitGroups = []
var numFGroups = 5 #Number of fruit groups that are spawned in the map
var lastGroups = -1

var currentFGroup
var wr

export (bool) var mult = false

func _ready():
	#Remove pellets
	#$Map/Pellets.queue_free()
	
	#Adjust players
	p1wr = weakref(player1)
	p2wr = weakref(player2)
	
	if Global.playerType1 == 0:
		player1.queue_free()
	elif Global.playerType1 == 2:
		player1.AI = true
	
	if Global.playerType2 == 0:
		player2.queue_free()
	elif Global.playerType2 == 2:
		player2.AI = true
		
	#Instance maps
	var m = load("res://Scenes/Maps/Map"+str(Global.selectedMap)+".tscn")
	m = m.instance()
	map.add_child(m)
	
	#Load fruit groups
	fruitGroups.resize(numFGroups)
	for x in numFGroups:
		fruitGroups[x] = load("res://Scenes/FruitGroups/FruitGroup("+str(Global.selectedMap)+" - "+str(x)+").tscn")
	
	#randomize and instance fruit group
	newFruits()
	
	#Fix pos and fade
	if Global.fixPos:
		self.set_position(Vector2(Global.dist,0))
	Global.fade.fadeIn()
	pass

func spawnPowerup():
	if !mult || get_tree().is_network_server():
		#New powerup
		var r = randi()%Global.powerups.size()
		var p = Global.powerups[r].instance()
		map.add_child(p)
		powerupWR = weakref(p)
		#New position
		var x = randi()%24
		var y = randi()%20
		p.position = Vector2((x*60)+30,(y*60)+30)
		if get_tree().is_network_server():
			Network.spawn_powerup(r,x,y)
	pass

func clientSpawnPowerup(r,x,y):
	#New powerup
	var p = Global.powerups[r].instance()
	map.add_child(p)
	powerupWR = weakref(p)
	#New position
	p.position = Vector2((x*60)+30,(y*60)+30)

func newFruits():
	if !mult || get_tree().is_network_server():
		var r = randi()%numFGroups
		#Generate new fruits
		if lastGroups == r:
			newFruits()
			return
		#Generate fruits
		else:
			lastGroups = r
			currentFGroup = fruitGroups[r].instance()
			map.add_child(currentFGroup)
			wr = weakref(currentFGroup)
			if get_tree().is_network_server():
				Network.spawn_fruit(r)

func clientNewFruits(r):
	lastGroups = r
	currentFGroup = fruitGroups[r].instance()
	map.add_child(currentFGroup)
	wr = weakref(currentFGroup)

func _process(delta):
	#Spawn new fruit group
	if (wr != null && !wr.get_ref()):
		newFruits()
	
	if (powerupWR != null && !powerupWR.get_ref() && $PowerupTimer.is_stopped()):
		$PowerupTimer.start()
	
	#End match - Versus game
	if (p2wr.get_ref() && p1wr.get_ref()):
		if !player1.alive && !player2.alive:
			gameOver.setSubText("Both players lost!", self.modulate)
		elif player1.alive && !player2.alive:
			gameOver.setSubText("Player 1 won!", player1.modulate)
		elif player2.alive && !player1.alive:
			gameOver.setSubText("Player 2 won!", player2.modulate)
	#Solo game
	else:
		if p1wr.get_ref() && !player1.alive:
			gameOver.setSubText("Game Over!", player1.modulate)
		elif p2wr.get_ref() && !player2.alive:
			gameOver.setSubText("Game Over!", player2.modulate)
	pass

func _on_PowerupTimer_timeout():
	spawnPowerup()
	pass

