extends Control

onready var nextPanel = $Next/SelectPanel
onready var exitPanel = $Exit/SelectPanel

onready var nextButton = $Next/NextButton
onready var exitButton = $Exit/ExitButton

onready var subTitle = $SubTitle

var row = 0

func _ready():
	nextButton.connect("pressed",self,"Next")
	exitButton.connect("pressed",self,"Exit")
	
	if Global.fixPos:
		self.set_position(Vector2(Global.dist,0))
	pass

func setSubText(text, playerColor):
	subTitle.modulate = playerColor
	subTitle.text = text
	self.show()
	get_tree().paused = true

func _process(delta):
	#Se estiver ativo
	if self.is_visible_in_tree():
		#Controla select panel
		if Input.is_action_just_pressed("ui_up") || Input.is_action_just_pressed("ui_W"):
			row -= 1
			if row <= -1:
				row = 1
		if Input.is_action_just_pressed("ui_down") || Input.is_action_just_pressed("ui_S"):
			row += 1
			if row >= 2:
				row = 0
		
		if nextButton.is_hovered():
			row = 0
		elif exitButton.is_hovered():
			row = 1
		
		#Move panel
		if row == 0:
			nextPanel.show()
			exitPanel.hide()
			#selectPanel.rect_position = nextButton.rect_position + Vector2(-10,-10)
		elif row == 1:
			nextPanel.hide()
			exitPanel.show()
			#selectPanel.rect_position = exitButton.rect_position + Vector2(-10,-10)
		
		#Activate selected button
		if Input.is_action_just_pressed("ui_select"):
			#Resume
			if (row == 0):
				Next()
			elif (row == 1):
				Exit()
	
	pass

func Next():
	get_tree().paused = false
	if !get_node("..").mult:
		Global.fade.fadeOut("res://Scenes/Game.tscn")
	else:
		Network.StartGame(Global.selectedMap)
		Global.fade.fadeOut("res://Scenes/MultiplayerGame.tscn")

func Exit():
	get_tree().paused = false
	Global.fade.fadeOut("res://Scenes/Menus/MainMenu.tscn")
