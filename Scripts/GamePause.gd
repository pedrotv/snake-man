extends Control

onready var selectPanel = $SelectPanel

onready var resumeButton = $Resume/ResumeButton
onready var optionsButton = $Options/OptionsButton
onready var exitButton = $Exit/ExitButton

var row = 0
var gameOver

func _ready():
	resumeButton.connect("pressed",self,"Resume")
	exitButton.connect("pressed",self,"Exit")
	
	gameOver = get_node("../GameOver")
	
	if Global.fixPos:
		self.set_position(Vector2(Global.dist,0))
	pass

func _process(delta):
	#Pausa/despausa
	if Input.is_action_just_pressed("ui_cancel") && !gameOver.visible:
		if self.is_visible_in_tree():
			self.hide()
			get_tree().paused = false
		else:
			self.show()
			get_tree().paused = true
	
	#Se estiver ativo
	if self.is_visible_in_tree():
		#Controla select panel
		if Input.is_action_just_pressed("ui_up") || Input.is_action_just_pressed("ui_W"):
			row -= 1
			if row <= -1:
				row = 2
		if Input.is_action_just_pressed("ui_down") || Input.is_action_just_pressed("ui_S"):
			row += 1
			if row >= 3:
				row = 0
		
		if resumeButton.is_hovered():
			row = 0
		elif optionsButton.is_hovered():
			row = 1
		elif exitButton.is_hovered():
			row = 2
		
		#Move panel
		if row == 0:
			selectPanel.rect_position = resumeButton.rect_position + Vector2(-10,-10)
		elif row == 1:
			selectPanel.rect_position = optionsButton.rect_position + Vector2(-10,-10)
		elif row == 2:
			selectPanel.rect_position = exitButton.rect_position + Vector2(-10,-10)
		
		#Activate selected button
		if Input.is_action_just_pressed("ui_select"):
			#Resume
			if (row == 0):
				Resume()
			elif (row == 2):
				Exit()
	
	pass

func Resume():
	self.hide()
	get_tree().paused = false

func Exit():
	get_tree().paused = false
	Global.fade.fadeOut("res://Scenes/Menus/MainMenu.tscn")
