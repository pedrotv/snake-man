extends Node

var fadeScene = preload("res://Scenes/Fade.tscn")
var musicScene = preload("res://Scenes/MainMenuMusic.tscn")
var bordersScene = preload("res://Scenes/Borders.tscn")

var invisible = false

var music
var fade
var border
var selectedMap
var dist
var fruitsScenes = []
var fixPos = false

var powerups = []

var playerType1 
var playerType2

func _ready():
	randomize()
	
	#Instace Fade
	fade = fadeScene.instance()
	add_child(fade)
	music = musicScene.instance()
	add_child(music)
	
	fruitsScenes.resize(10)
	fruitsScenes[0] = load("res://Scenes/Fruits/BigPellet.tscn")
	fruitsScenes[1] = load("res://Scenes/Fruits/Orange.tscn")
	fruitsScenes[2] = load("res://Scenes/Fruits/Tomato.tscn")
	fruitsScenes[3] = load("res://Scenes/Fruits/Cucumber.tscn")
	fruitsScenes[4] = load("res://Scenes/Fruits/Grape.tscn")
	fruitsScenes[5] = load("res://Scenes/Fruits/Brocolli.tscn")
	fruitsScenes[6] = load("res://Scenes/Fruits/Pineaplee.tscn")
	fruitsScenes[7] = load("res://Scenes/Fruits/Carrot.tscn")
	fruitsScenes[8] = load("res://Scenes/Fruits/Watermellon.tscn")
	fruitsScenes[9] = load("res://Scenes/Fruits/Apple.tscn")
	
	dist = (get_viewport().get_visible_rect().size.x - 1080)/2
	
	powerups.resize(3)
	powerups[0] = load("res://Scenes/Fruits/Speed.tscn")
	powerups[1] = load("res://Scenes/Fruits/Spike.tscn")
	powerups[2] = load("res://Scenes/Fruits/Metal.tscn")
	
	pass 

func spawnFruit(x,y):
	#var r = randi()%fruitsScenes.size()
	var fruits = fruitsScenes[0].instance()
	get_node("/root/Control/Game/Fruits").add_child(fruits)
	fruits.position = Vector2(x*60 + 30, y*60 + 30)
	pass

func _process(delta):
	if !music.playing:
		music.play()
	
	#Fullscreen
	if Input.is_action_pressed("ALT"):
		if Input.is_action_just_pressed("ui_fullscreen"):
			OS.window_fullscreen = !OS.window_fullscreen
	
	if Input.is_action_just_pressed("ui_page_down"):
		get_tree().quit()
	pass
