extends Control

onready var createPanel = $Create/CreatePanel
onready var joinPanel = $Join/JoinPanel
onready var backPanel = $Back/BackPanel

onready var createButton = $Create/CreateButton
onready var joinButton = $Join/JoinButton
onready var backButton = $Back/BackButton

onready var nameEdit = $TextEditName
onready var IPEdit = $TextEditIP

var buttonNum = 0
var playerName = " "
var ip = "127.0.0.1"

func _ready():
	createButton.connect("pressed",self,"CreateServer")
	joinButton.connect("pressed",self,"JoinServer")
	backButton.connect("pressed",self,"PrevScreen")
	
	if Global.fixPos:
		self.set_position(Vector2(Global.dist,0))
	Global.fade.fadeIn()
	pass 

func _process(delta):
	#Control panel
	if Input.is_action_just_pressed("ui_up") || Input.is_action_just_pressed("ui_W"):
		buttonNum -= 1
		if buttonNum <= -1:
			buttonNum = 2
	if Input.is_action_just_pressed("ui_down") || Input.is_action_just_pressed("ui_S"):
		buttonNum += 1
		if buttonNum >= 3:
			buttonNum = 0
	
	if createButton.is_hovered():
		buttonNum = 0
	elif joinButton.is_hovered():
		buttonNum = 1
	elif backButton.is_hovered():
		buttonNum = 2
	
	createPanel.hide()
	joinPanel.hide()
	backPanel.hide()
	
	if buttonNum == 0:
		createPanel.show()
	elif buttonNum == 1:
		joinPanel.show()
	elif buttonNum == 2:
		backPanel.show()
	
	#Activate selected button
	if Input.is_action_just_pressed("ui_select"):
		if buttonNum == 0:
			CreateServer() 
		elif buttonNum == 1:
			JoinServer()
		elif buttonNum == 2:
			PrevScreen()
	
	if Input.is_action_just_pressed("ui_cancel"):
		PrevScreen()

func CreateServer():
	Network.create_server(playerName)
	Global.fade.fadeOut("res://Scenes/Multiplayer/MPMapSelect.tscn")

func JoinServer():
	Network.connect_to_server(playerName,ip)
	Global.fade.fadeOut("res://Scenes/Multiplayer/MPMapSelect.tscn")

func PrevScreen():
	Global.fade.fadeOut("res://Scenes/Menus/MainMenu.tscn")

func _on_TextEditName_text_changed(new_text):
	playerName = new_text
	pass

func _on_TextEditIP_text_changed(new_text):
	ip = new_text
	pass
