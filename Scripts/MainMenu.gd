extends Control

#Buttons
onready var localButton = $Buttons/LocalButton/Local
onready var onlineButton = $Buttons/OnlineButton/Online
onready var configButton = $Buttons/ConfigurationsButton/Configurations
onready var exitButton = $Buttons/ExitButton/Exit

#Arrow
var selectPanel = []

#Selected Button
var buttonNum = 0

func _ready():
	localButton.connect("pressed", self, "PlayLocal")
	onlineButton.connect("pressed", self, "PlayMultiplayer")
	exitButton.connect("pressed",self, "ExitGame")
	
	selectPanel.resize(4)
	selectPanel[0] = $Buttons/SelectPanel0
	selectPanel[1] = $Buttons/SelectPanel1
	selectPanel[2] = $Buttons/SelectPanel2
	selectPanel[3] = $Buttons/SelectPanel3
	
	if Global.fixPos:
		self.set_position(Vector2(Global.dist,0))
	Global.fade.fadeIn()
	pass

func _process(delta):
	#Control arrow
	if Input.is_action_just_pressed("ui_up") || Input.is_action_just_pressed("ui_W"):
		buttonNum -= 2
		if buttonNum <= -1:
			buttonNum += 4
	if Input.is_action_just_pressed("ui_down") || Input.is_action_just_pressed("ui_S"):
		buttonNum += 2
		if buttonNum >= 4:
			buttonNum -= 4
	if Input.is_action_just_pressed("ui_left") || Input.is_action_just_pressed("ui_A"):
		if buttonNum == 0 || buttonNum == 2:
			buttonNum += 1
		else:
			buttonNum -= 1
	if Input.is_action_just_pressed("ui_right") || Input.is_action_just_pressed("ui_D"):
		if buttonNum == 0 || buttonNum == 2:
			buttonNum += 1
		else:
			buttonNum -= 1
	
	#If button is hovered
	if localButton.is_hovered():
		buttonNum = 0
	if onlineButton.is_hovered():
		buttonNum = 1
	if configButton.is_hovered():
		buttonNum = 2
	if exitButton.is_hovered():
		buttonNum = 3
	
	#Move arrow
	for x in range(4):
		selectPanel[x].hide()
	selectPanel[buttonNum].show()
	
	#Activate selected button
	if Input.is_action_just_pressed("ui_select"):
		if buttonNum == 0:
			PlayLocal()
		if buttonNum == 1:
			PlayMultiplayer()
		elif buttonNum == 3:
			ExitGame()
	
	pass

func PlayLocal():
	Global.fade.fadeOut("res://Scenes/Menus/PlayerSelect.tscn")
	pass

func PlayMultiplayer():
	Global.fade.fadeOut("res://Scenes/Multiplayer/MPPlayerSelect.tscn")
	pass

func ExitGame():
	Global.fade.fadeOut("Quit")
	pass

