extends Control

onready var NextPanel = $Next/NextPanel
onready var NextButton = $Next/NextButton

onready var BackPanel = $Back/BackPanel
onready var BackButton = $Back/BackButton

onready var LeftPanel = $LeftArrow/NextPanel
onready var LeftButton = $LeftArrow/NextButton
onready var RightPanel = $RightArrow/NextPanel
onready var RightButton = $RightArrow/NextButton

var maps = []

var map = 0
var buttonNum = 0

func _ready():
	NextButton.connect("pressed",self,"NextScreen")
	BackButton.connect("pressed",self,"PrevScreen")
	LeftButton.connect("pressed",self,"PrevMap")
	RightButton.connect("pressed",self,"NextMap")
	
	maps.resize(3)
	maps[0] = $Maps/Map0
	maps[1] = $Maps/Map1
	maps[2] = $Maps/Map2
	
	if Global.fixPos:
		self.set_position(Vector2(Global.dist,0))
	Global.fade.fadeIn()
	pass

func _process(delta):
	#Control panel
	if Input.is_action_just_pressed("ui_up") || Input.is_action_just_pressed("ui_W"):
		buttonNum -= 1
		if buttonNum <= -1:
			buttonNum = 2
	if Input.is_action_just_pressed("ui_down") || Input.is_action_just_pressed("ui_S"):
		buttonNum += 1
		if buttonNum >= 3:
			buttonNum = 0
	if Input.is_action_just_pressed("ui_left") || Input.is_action_just_pressed("ui_A"):
		if buttonNum ==0:
			PrevMap()
	if Input.is_action_just_pressed("ui_right") || Input.is_action_just_pressed("ui_D"):
		if buttonNum == 0:
			NextMap()
	
	if LeftButton.is_hovered() || RightButton.is_hovered():
		buttonNum = 0
	if NextButton.is_hovered():
		buttonNum = 1
	if BackButton.is_hovered():
		buttonNum = 2
	
	#Move arrow
	if buttonNum == 1:
		LeftPanel.hide()
		RightPanel.hide()
		BackPanel.hide()
		NextPanel.show()
	elif buttonNum == 2:
		LeftPanel.hide()
		RightPanel.hide()
		NextPanel.hide()
		BackPanel.show()
	else:
		LeftPanel.show()
		RightPanel.show()
		NextPanel.hide()
		BackPanel.hide()
	
	#Show map
	for x in range (3):
		maps[x].hide()
	maps[map].show()
	
	#Activate selected button
	if Input.is_action_just_pressed("ui_select"):
		if buttonNum == 1:
			NextScreen() 
		if buttonNum == 2:
			PrevScreen()
	
	if Input.is_action_just_pressed("ui_cancel"):
		PrevScreen()
	
	pass

#Play game (load map/create character)
func NextScreen():
	Global.selectedMap = map
	Global.fade.fadeOut("res://Scenes/Game.tscn")

func PrevScreen():
	Global.fade.fadeOut("res://Scenes/Menus/PlayerSelect.tscn")

func NextMap():
	map += 1
	if map >= 3:
		map = 0

func PrevMap():
	map -= 1
	if map <= -1:
		map = 2