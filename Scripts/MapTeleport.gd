extends TileMap

func teleport(body, posX, posY):
	#print(body.name)
	if body.is_in_group("Player"):
		body.position.x = posX
		body.position.y = posY
	if body.is_in_group("Body"):
		body.position.x = posX
		body.position.y = posY
	pass

func _on_LeftArea_body_shape_entered(body_id, body, body_shape, area_shape):
	teleport(body,1470,body.position.y)
	pass

func _on_RightArea_body_shape_entered(body_id, body, body_shape, area_shape):
	teleport(body,-30,body.position.y)
	pass

func _on_UpArea_body_shape_entered(body_id, body, body_shape, area_shape):
	teleport(body,body.position.x,1230)
	pass

func _on_DownArea_body_shape_entered(body_id, body, body_shape, area_shape):
	teleport(body,body.position.x,-30)
	pass
	
func _on_PreArea_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("Player") || body.is_in_group("Body"):
		body.tp = true
	pass

func _on_PreArea_body_shape_exited(body_id, body, body_shape, area_shape):
	if body != null:
		if body.is_in_group("Player") || body.is_in_group("Body"):
			body.tp = false
	pass