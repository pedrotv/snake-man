extends Control

onready var CreatePanel = $Create/CreatePanel
onready var CreateButton = $Create/CreateButton

onready var JoinPanel = $Join/JoinPanel
onready var JoinButton = $Join/JoinButton

onready var BackPanel = $Back/BackPanel
onready var BackButton = $Back/BackButton

onready var LeftPanel = $LeftArrow/NextPanel
onready var LeftButton = $LeftArrow/NextButton
onready var RightPanel = $RightArrow/NextPanel
onready var RightButton = $RightArrow/NextButton

var maps = []

var map = 0
var buttonNum = 0

func _ready():
	CreateButton.connect("pressed",self,"CreateGame")
	JoinButton.connect("pressed",self,"JoinGame")
	LeftButton.connect("pressed",self,"PrevMap")
	RightButton.connect("pressed",self,"NextMap")
	
	maps.resize(3)
	maps[0] = $Maps/Map0
	maps[1] = $Maps/Map1
	maps[2] = $Maps/Map2
	
	if Global.fixPos:
		self.set_position(Vector2(Global.dist,0))
	Global.fade.fadeIn()
	pass

func _process(delta):
	#Control panel
	if Input.is_action_just_pressed("ui_up") || Input.is_action_just_pressed("ui_W"):
		buttonNum -= 1
		if buttonNum <= -1:
			buttonNum = 3
	if Input.is_action_just_pressed("ui_down") || Input.is_action_just_pressed("ui_S"):
		buttonNum += 1
		if buttonNum >= 4:
			buttonNum = 0
	if Input.is_action_just_pressed("ui_left") || Input.is_action_just_pressed("ui_A"):
		if buttonNum ==0:
			PrevMap()
	if Input.is_action_just_pressed("ui_right") || Input.is_action_just_pressed("ui_D"):
		if buttonNum == 0:
			NextMap()
	
	if LeftButton.is_hovered() || RightButton.is_hovered():
		buttonNum = 0
	if CreateButton.is_hovered():
		buttonNum = 1
	if JoinButton.is_hovered():
		buttonNum = 2
	if BackButton.is_hovered():
		buttonNum = 3
	
	#Move arrow
	if buttonNum == 1:
		LeftPanel.hide()
		RightPanel.hide()
		BackPanel.hide()
		CreatePanel.show()
		JoinPanel.hide()
	elif buttonNum == 2:
		LeftPanel.hide()
		RightPanel.hide()
		BackPanel.hide()
		CreatePanel.hide()
		JoinPanel.show()
	elif buttonNum == 3:
		LeftPanel.hide()
		RightPanel.hide()
		BackPanel.show()
		CreatePanel.hide()
		JoinPanel.hide()
	else:
		LeftPanel.show()
		RightPanel.show()
		BackPanel.hide()
		CreatePanel.hide()
		JoinPanel.hide()
	
	#Show map
	for x in range (3):
		maps[x].hide()
	maps[map].show()
	
	#Activate selected button
	if Input.is_action_just_pressed("ui_select"):
		if buttonNum == 1:
			CreateGame() 
		if buttonNum == 2:
			JoinGame()
		if buttonNum == 3:
			PrevScreen()
	
	if Input.is_action_just_pressed("ui_cancel"):
		PrevScreen()
	
	pass

func CreateGame():
	Network.create_server("P1")
	NextScreen()

func JoinGame():
	Network.connect_to_server("P2")
	NextScreen()

func NextScreen():
	Global.selectedMap = map
	Global.fade.fadeOut("res://Scenes/GameMult.tscn")

func PrevScreen():
	Global.fade.fadeOut("res://Scenes/Menus/MainMenu.tscn")

func NextMap():
	map += 1
	if map >= 3:
		map = 0

func PrevMap():
	map -= 1
	if map <= -1:
		map = 2