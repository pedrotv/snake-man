extends Node

#const DEFAULT_IP = '127.0.0.1'
const DEFAULT_PORT = 4242 #31400
const MAX_PLAYERS = 2

var players = []
var self_data = { name = '', id = -1 , _pMove = null}

signal player_disconnected
signal server_disconnected

func _ready():
	get_tree().connect('network_peer_disconnected', self, '_on_player_disconnected')
	players.resize(2)

func create_server(player_nickname):
	self_data.name = player_nickname
	self_data.id = 0
	players[0] = self_data
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(DEFAULT_PORT, MAX_PLAYERS)
	get_tree().set_network_peer(peer)

func connect_to_server(player_nickname,ip):
	self_data.name = player_nickname
	self_data.id = 1
	get_tree().connect('connected_to_server', self, '_connected_to_server')
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(ip, DEFAULT_PORT)
	get_tree().set_network_peer(peer)

#As soon as the peer connects to server
func _connected_to_server():
	players[1] = self_data
	rpc('_send_player_info', 1, self_data)

func _on_player_disconnected(id):
	players.erase(id)

remote func _send_player_info(id, info):
	players[id] = info
	if (get_tree().is_network_server()):
		rpc('_send_player_info', 0, self_data)

#Start game on all players
func StartGame(map):
	rpc('_start_game', map)

remote func _start_game(map):
	Global.selectedMap = map
	Global.fade.fadeOut("res://Scenes/MultiplayerGame.tscn")

#Update player position
func update_player(id, direction,h,v,lastPos):
	rpc('_send_player_update',id, direction,h,v,lastPos)

remote func _send_player_update(id, direction,h,v,lastPos):
	get_tree().get_current_scene().get_node("Game/Player"+str(id+1)).chooseDirection(direction,h,v)
	get_tree().get_current_scene().get_node("Game/Player"+str(id+1)).lastPos = lastPos

#Spawn fruit on client
func spawn_fruit(r):
	rpc('_spawn_fruit',r)

remote func _spawn_fruit(r):
	get_tree().get_current_scene().get_node("Game").clientNewFruits(r)

#Spawn powerup on client
func spawn_powerup(r,x,y):
	rpc('_spawn_powerup',r,x,y)

remote func _spawn_powerup(r,x,y):
	get_tree().get_current_scene().get_node("Game").clientSpawnPowerup(r,x,y)






