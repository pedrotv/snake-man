extends Control

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
	
	if Input.is_action_just_pressed("ui_home"):
		if get_tree().paused:
			get_tree().paused = false
		else:
			get_tree().paused = true
	pass
