extends Area2D

func _on_Pellet_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("Player"):
		body.score += 10
		body.pellets += 1
		self.queue_free()
	if body.is_in_group("Fruit"):
		self.queue_free()
	pass