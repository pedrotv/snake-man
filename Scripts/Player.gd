extends KinematicBody2D

var speed = 200
var object_hit 

var horizontal = 0
var vertical = 0

var tile = Vector2()
var percent = Vector2()

var lastPos = []

var score = 0
var pellets = 0
var maxPellets = 15

onready var bodyPrefab = preload("res://Scenes/Player/PlayerBody.tscn")
onready var spikePrefab = preload("res://Scenes/Player/SpikeBody.tscn")
onready var timer = $MoveTimer
var scoreLabel

var child #The first body  part
var endBody #The last body part
var body #Body node
var childWR
var game
var tfwr

var metal = false
var stopped = false
var canMove = true
var alive = true
var tp = false
var active = true
var spike = false
var AI = false
var deadzone = 0.15
var nextMove = 0 #Clockwise
var color
var bcolor

#AI
var distance = 1000
var targetFruit = null
var targetPos = Vector2()

export (int) var numID

func _ready():
	#print(get_collision_mask())
	lastPos.resize(3)
	for x in lastPos.size():
		lastPos[x] = Vector2(0,0)
	
	findTile()
	game = get_node("../../Game")
	body = get_node("../Body"+str(numID))
	scoreLabel = get_node("../P"+str(numID)+"Score")
	if numID == 1:
		vertical = 1
		nextMove = 3
	else:
		vertical = -1
		nextMove = 1
	pass

func chooseDirection(rot, h,v):
	self.rotation_degrees = rot
	horizontal = h
	vertical = v
	canMove = false
	timer.start()

func findTile():
	#Find current tile
	tile.x = position.x - 30
	percent.x = tile.x/60 - round(tile.x/60)
	tile.x = round(tile.x/60)
	tile.y = position.y - 30
	percent.y = tile.y/60 - round(tile.y/60)
	tile.y = round(tile.y/60)

func setLastPos():
	if tile != lastPos[0]:
		lastPos.push_front(tile)
		lastPos.pop_back()

func _process(delta):
	if active:
		if speed >= 301:
			speed == 300
		
		scoreLabel.text = "Player "+str(numID)+" Score: " + str(score)
		
		if pellets >= maxPellets:
			newBody()
			pellets = 0
		
		if spike:
			newBody()
		
		if metal:
			metal()
		
		findTile()
		
		#Movement
		if canMove && !tp:
			
			if AI: #AI controled
				AIcontrol()
			else: #Player controled
				playerControl()
		
		#Fix player on tile
		if horizontal == 0:
			position.x = tile.x * 60 + 30
		if vertical == 0:
			position.y = tile.y * 60 + 30
		
		#IDK
		var sum = sqrt(abs(horizontal) + abs(vertical))
		if sum == 0:
			sum = 1
		
		#Move Player and collide
		object_hit = move_and_collide(Vector2(speed * delta * horizontal/sum, speed * delta * vertical/sum))
		
		checkCollision()
	pass

func checkCollision():
	#Get object that collided 
	if object_hit != null:
		object_hit = object_hit.get_collider()
		
		#Kill player if he collides with a wall
		if object_hit.is_in_group("Map"):
			alive = false
		
		#Kill both players if they collide
		if object_hit.is_in_group("Player"):
			if !Global.invisible:
				if body.get_child_count() > object_hit.body.get_child_count():
					object_hit.alive = false
				elif body.get_child_count() == object_hit.body.get_child_count():
					object_hit.alive = false
					alive = false
				else:
					alive = false

func playerControl():
	if Global.playerType1 == 1 && Global.playerType2 == 1:
		if numID == 1:
			if horizontal != 0:
				if Input.is_action_pressed("ui_S"):
					nextMove = 3
				if Input.is_action_pressed("ui_W"):
					nextMove = 1
			if vertical != 0:
				if Input.is_action_pressed("ui_A"):
					nextMove = 4
				if Input.is_action_pressed("ui_D"):
					nextMove = 2
		else:
			if horizontal != 0:
				if Input.is_action_pressed("ui_down"):
					nextMove = 3
				if Input.is_action_pressed("ui_up"):
					nextMove = 1
			if vertical != 0:
				if Input.is_action_pressed("ui_left"):
					nextMove = 4
				if Input.is_action_pressed("ui_right"):
					nextMove = 2
	else:
		if horizontal != 0:
			if Input.is_action_pressed("ui_S") || Input.is_action_pressed("ui_down"):
				nextMove = 3
			if Input.is_action_pressed("ui_W") || Input.is_action_pressed("ui_up"):
				nextMove = 1
		if vertical != 0:
			if Input.is_action_pressed("ui_A") || Input.is_action_pressed("ui_left"):
				nextMove = 4
			if Input.is_action_pressed("ui_D") || Input.is_action_pressed("ui_right"):
				nextMove = 2
	
	if percent.x >= -deadzone && percent.x <= deadzone && percent.y >= -deadzone && percent.y <= deadzone:
		if nextMove == 1:
			chooseDirection(180,0,-1)
			nextMove = 0
		elif nextMove == 2:
			chooseDirection(270,1,0)
			nextMove = 0
		elif nextMove == 3:
			chooseDirection(0,0,1)
			nextMove = 0
		elif nextMove == 4:
			chooseDirection(90,-1,0)
			nextMove = 0
		
		setLastPos()

func metal():
	if $MetalTimer.is_stopped():
		$MetalTimer.start()
		color = modulate
		bcolor = body.modulate
		modulate = Color(737373)
		body.modulate = Color(737373)
		Global.invisible = true
		set_collision_layer(0)
		set_collision_mask(20)
		pass

func newBody():
	#Instance body
	var pb
	if spike:
		pb = spikePrefab.instance()
		spike = false
	else:
		pb = bodyPrefab.instance()
	if endBody == null: #if there's no body
		child = self 
		childWR = weakref(child)
		pb.playerpath = self
	else:
		pb.playerpath = endBody
	body.add_child(pb)
	pb.readyBody() #Ready the body
	pb.player = self
	endBody = pb
	$SFX.play()
	pass

func _on_Timer_timeout():
	canMove = true
	pass

func _on_MetalTimer_timeout():
	metal = false
	modulate = color
	body.modulate = bcolor
	Global.invisible = false
	set_collision_layer(2)
	set_collision_mask(22)
	pass

#------------
#AI FUNCTIONS
#------------

func getallnodes(node):
	for N in node.get_children():
		if N.get_child_count() > 0:
			if N.is_in_group("Fruit"):
				findFruitTile(N)
			getallnodes(N)

func findFruitTile(N):
	var tileFruit = Vector2()
	
	#Find current tile
	tileFruit.x = N.position.x - 30
	tileFruit.x = round(tileFruit.x/60)
	tileFruit.y = N.position.y - 30
	tileFruit.y = round(tileFruit.y/60)
	
	#Se a fruta estiver mais perto
	var aux = Vector2(tileFruit.x - tile.x, tileFruit.y - tile.y)
	if distance > (abs(aux.x) + abs(aux.y)):
		distance = (abs(aux.x) + abs(aux.y))
		targetFruit = N
		targetPos = tileFruit
		tfwr = weakref(targetFruit)

func Astar(target):
	var aux = Vector2(targetPos.x - tile.x, targetPos.y - tile.y)
	
	if aux.y == 0:
		if aux.x > 0:
			nextMove = 2
		elif aux.x < 0:
			nextMove = 4 
	elif aux.x == 0:
		if aux.y > 0:
			nextMove = 3
		elif aux.y < 0:
			nextMove = 1 
	
	if percent.x >= -deadzone && percent.x <= deadzone && percent.y >= -deadzone && percent.y <= deadzone:
		if nextMove == 1:
			chooseDirection(180,0,-1)
			nextMove = 0
		elif nextMove == 2:
			chooseDirection(270,1,0)
			nextMove = 0
		elif nextMove == 3:
			chooseDirection(0,0,1)
			nextMove = 0
		elif nextMove == 4:
			chooseDirection(90,-1,0)
			nextMove = 0
		
		setLastPos()
	
	if aux.x == 0 && aux.y == 0:
		distance = 1000
		targetFruit = null
		targetPos = null
		
	pass

func AIcontrol():
	#Check all nodes
	getallnodes(game.map)
	
	#Make path to target
	Astar(targetFruit)