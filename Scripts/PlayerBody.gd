extends KinematicBody2D

var player
var parent
var child

var speed = 150
var object_hit 

var horizontal = 0
var vertical = 0

var tile = Vector2()
var percent = Vector2()

var lastPos = []

var deadzone = 0.2

onready var sprite = $Sprite

var playerpath
var wr
var childWR
var tp = false

var alive = true

func readyBody():
	#set_as_toplevel(true)
	lastPos.resize(3)
	for x in lastPos.size():
		lastPos[x] = Vector2(0,0)
	
	wr = weakref(playerpath)
	parent = playerpath
	
	if !(wr.get_ref()):
		#if parent.is_in_group("BigPellet"):
		queue_free()
	else:
		if parent.is_in_group("Player") || parent.is_in_group("Body"):
			speed = parent.speed
			horizontal = parent.horizontal
			vertical = parent.vertical
			tile = parent.tile
			position = parent.position
			#deadzone = parent.deadzone
			modulate = parent.modulate
			
			var spawnDist = 55
			if horizontal == 1:
				tile.x -= 1
				position.x -= spawnDist
			if horizontal == -1:
				tile.x += 1
				position.x += spawnDist
			if vertical == 1:
				tile.y -= 1
				position.y -= spawnDist
			if vertical == -1:
				tile.y += 1
				position.y += spawnDist
	
	pass

func findTile():
	tile.x = position.x - 30
	percent.x = tile.x/60 - round(tile.x/60)
	tile.x = round(tile.x/60)
	tile.y = position.y - 30
	percent.y = tile.y/60 - round(tile.y/60)
	tile.y = round(tile.y/60)
	
	pass

func setLastPos():
	if tile != lastPos[0]:
		lastPos.push_front(tile)
		lastPos.pop_back()

func _process(delta):
	if (!wr.get_ref()):
		Global.spawnFruit(tile.x,tile.y)
		queue_free()
	else:
		if alive:
			speed = parent.speed
		
		#Find current tile
		findTile()
		
		if !tp:
			if parent.percent.x >= -deadzone && parent.percent.x <= deadzone && parent.percent.y >= -deadzone && parent.percent.y <= deadzone:
				setLastPos()
				if !((parent.lastPos[0].y - lastPos[0].y) + (parent.lastPos[0].x - lastPos[0].x) == 0):
					vertical = parent.lastPos[0].y - lastPos[0].y
					horizontal = parent.lastPos[0].x - lastPos[0].x
		
		#Fix player on tile
		if horizontal == 0:
			position.x = tile.x * 60 + 30
		if vertical == 0:
			position.y = tile.y * 60 + 30
		
		#IDK
		var sum = sqrt(abs(horizontal) + abs(vertical))
		if sum == 0:
			sum = 1
		
		#Move Player and collide
		object_hit = move_and_collide(Vector2(speed * delta * horizontal/sum, speed * delta * vertical/sum))
	pass

func setTP():
	if childWR != null:
		if (childWR.get_ref()):
			if child != null:
				child.tp = true
	pass

func _on_Area2D_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("Player") && parent != body && !Global.invisible:
		if self.is_in_group("Spike"):
			body.alive = false
		else:
			player.endBody = parent
			alive = false
			self.queue_free()
	pass
