extends Control

onready var NextPanel = $Next/NextPanel
onready var NextButton = $Next/NextButton

onready var BackPanel = $Back/BackPanel
onready var BackButton = $Back/BackButton

onready var player1 = $Player1
onready var player2 = $Player2

onready var P1LPanel = $Player1/LeftArrow/NextPanel
onready var P1LButton = $Player1/LeftArrow/NextButton
onready var P1RPanel = $Player1/RightArrow/NextPanel
onready var P1RButton = $Player1/RightArrow/NextButton

onready var P2LPanel = $Player2/LeftArrow/NextPanel
onready var P2LButton = $Player2/LeftArrow/NextButton
onready var P2RPanel = $Player2/RightArrow/NextPanel
onready var P2RButton = $Player2/RightArrow/NextButton

onready var P1Label = $Player1/Label
onready var P2Label = $Player2/Label

var buttonNum = 0
var p1Type = 1
var p2Type = 0

func _ready():
	NextButton.connect("pressed",self,"NextScreen")
	BackButton.connect("pressed",self,"PrevScreen")
	P1LButton.connect("pressed",self,"PrevType",[1])
	P1RButton.connect("pressed",self,"NextType",[1])
	P2LButton.connect("pressed",self,"PrevType",[2])
	P2RButton.connect("pressed",self,"NextType",[2])
	
	if Global.fixPos:
		self.set_position(Vector2(Global.dist,0))
	Global.fade.fadeIn()
	pass

func _process(delta):
	#Control panel
	if Input.is_action_just_pressed("ui_up") || Input.is_action_just_pressed("ui_W"):
		buttonNum -= 1
		if buttonNum <= -1:
			buttonNum = 3
	if Input.is_action_just_pressed("ui_down") || Input.is_action_just_pressed("ui_S"):
		buttonNum += 1
		if buttonNum >= 4:
			buttonNum = 0
	if Input.is_action_just_pressed("ui_left") || Input.is_action_just_pressed("ui_A"):
		if buttonNum == 0 || buttonNum == 1:
			PrevType(buttonNum+1)
	if Input.is_action_just_pressed("ui_right") || Input.is_action_just_pressed("ui_D"):
		if buttonNum == 0 || buttonNum == 1:
			NextType(buttonNum+1)
	
	if P1LButton.is_hovered() || P1RButton.is_hovered():
		buttonNum = 0
	if P2LButton.is_hovered() || P2RButton.is_hovered():
		buttonNum = 1
	if NextButton.is_hovered():
		buttonNum = 2
	if BackButton.is_hovered():
		buttonNum = 3
	
	P1LPanel.hide()
	P1RPanel.hide()
	P2LPanel.hide()
	P2RPanel.hide()
	NextPanel.hide()
	BackPanel.hide()
	
	#Move arrow
	if buttonNum == 0:
		P1LPanel.show()
		P1RPanel.show()
	elif buttonNum == 1:
		P2LPanel.show()
		P2RPanel.show()
	elif buttonNum == 2:
		NextPanel.show()
	else:
		BackPanel.show()
	
	#Activate selected button
	if Input.is_action_just_pressed("ui_select"):
		if buttonNum == 2:
			NextScreen() 
		if buttonNum == 3:
			PrevScreen()
	
	if Input.is_action_just_pressed("ui_cancel"):
		PrevScreen()
	
	if p1Type == 0:
		P1Label.text = "None"
		P1Label.modulate = Color(1, 1, 1)
	elif p1Type == 1:
		P1Label.text = "Player 1"
		P1Label.modulate = Color(1, 0.690196, 0)
	elif p1Type == 2:
		P1Label.text = "CPU"
		P1Label.modulate = Color(0.65, 0.65, 0.65)
	
	if p2Type == 0:
		P2Label.text = "None"
		P2Label.modulate = Color(1, 1, 1)
	elif p2Type == 1:
		P2Label.text = "Player 2"
		P2Label.modulate = Color(0.839216, 0, 0)
	elif p2Type == 2:
		P2Label.text = "CPU"
		P2Label.modulate = Color(0.65, 0.65, 0.65)
	
	pass

#Play game (load map/create character)
func NextScreen():
	Global.playerType1 = p1Type
	Global.playerType2 = p2Type
	if p1Type + p2Type >= 1:
		Global.fade.fadeOut("res://Scenes/Menus/MapSelect.tscn")

func PrevScreen():
	Global.fade.fadeOut("res://Scenes/Menus/MainMenu.tscn")

func NextType(n):
	if n == 1:
		p1Type += 1
		if p1Type >= 3:
			p1Type = 0
	else:
		p2Type += 1
		if p2Type >= 3:
			p2Type = 0

func PrevType(n):
	if n == 1:
		p1Type -= 1
		if p1Type <= -1:
			p1Type = 2
	else:
		p2Type -= 1
		if p2Type <= -1:
			p2Type = 2