extends Area2D

export (String) var type 

func _on_Powerup_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("Map"):
		get_node("../..").spawnPowerup()
		self.queue_free()
	if body.is_in_group("Player"):
		if type == "Speed":
			body.speed += 15
			self.queue_free()
		if type == "Spike":
			body.spike = true
			self.queue_free()
		if type == "Metal":
			body.metal = true
			self.queue_free()
	pass 

func _process(delta):
	#show()
	pass